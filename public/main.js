var firebase;

$(function () {
  var $name = $('#name'),
    $content = $('#content'),
    $btn = $('#btn'),
    $show = $('#show'),
    ms = new Date().getTime();


  var database = firebase.database().ref();
  var user = firebase.auth().currentUser;
  $btn.on('click', write);
  $content.on('keydown', function (e) {
    if (e.keyCode == 13) {
      write();
    }
  });

  function write() {
    var user = firebase.auth().currentUser;
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }
    var now = h + ':' + m + ':' + s;
    var postData = {
      name: user.displayName,
      content: $('#content').val(),
      time: now,
      id: 'id' + ms
    };
    database.push(postData);
    $content.val('');
  }

  database.once('value', function (snapshot) {
    $show.html('');
    for (var i in snapshot.val()) {
      $show.append('<div><div class="time">' + snapshot.val()[i].time + '</div><div class="name">' + snapshot.val()[i].name + ' 說</div><div class="content">' + snapshot.val()[i].content + '</div>');
    }
    $show.scrollTop($show[0].scrollHeight);
  });

  database.limitToLast(1).on('value', function (snapshot) {
    for (var i in snapshot.val()) {
      $show.append('<div class="' + snapshot.val()[i].id + '"><div class="time">' + snapshot.val()[i].time + '</div><div class="name">' + snapshot.val()[i].name + ' 說</div><div class="content">' + snapshot.val()[i].content + '</div>');
    }
    $show.scrollTop($show[0].scrollHeight);
    $show.find('.id' + ms + ' .name').css({
      'float': 'right',
      'padding-top': '12px',
      'color': '#fc0'
    });
    $show.find('.id' + ms + ' .content').css({
      'float': 'right',
      'margin-right': '10px'
    });
    $show.find('.id' + ms + ' .time').css({
      'right': '0',
      'color': '#777'
    });
  });
  //
  var user_email = '';
  firebase.auth().onAuthStateChanged(function (user) {
    var log = document.getElementById('logged');
    if (user) {
      user_email = user.email;
      log.innerHTML='<button id="logout-btn">Logout</button>'+user_email;
      var logout_button = document.getElementById('logout-btn');
      logout_button.addEventListener('click', function () {
        firebase.auth().signOut()
          .then(function () {
            alert('Sign Out!')
            window.location.replace('chat.html')
          })
          .catch(function (error) {
            alert('Sign Out Error!')
          });
      });
    } else {
      menu.innerHTML = "<a class='dropdown-item' href='login.html'>Login</a>";
      document.getElementById('post_list').innerHTML = "";
    }
  });


});

$(function () {
  'use strict';

  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open');
  });
});

document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.');
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe() {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('SUITANGTANG Official', {
      icon: 'images/notification.png',
      body: "Pay Successfully!!!\nThe Latest Shopping History Is Below The Page, Click And Understang Us More.",
    });
    notification.onclick = function () {
      window.location.replace('about.html');
    };
  }
}
document.getElementById('ch').addEventListener('click',function(){
  var user = firebase.auth().currentUser;
  var change = document.getElementById("paul").value;
  user.updateProfile({
    displayName:change
  }).then(function() {
     window.alert("Welcome  "+ user.displayName);
     window.location.replace("chat.html");
     //document.getElementById("user_para").innerHTML = "Welcome User : " + user.displayName;
  }).catch(function(error) {
      window.alert("wrong");
  });
});
function loggin(){

  window.location.replace("login.html");
}