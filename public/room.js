var RoomCode = "public";

// Get the modal
var modalchat = document.getElementById('myModal3');

// When the user clicks the button, open the modal 
$('#content').on('keydown', function (event) {
    if (event.keyCode == 27) {
        modalchat.style.display = "none";
    }
});
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modalchat) {
        modalchat.style.display = "none";
    }
}
function ExchageRoomCode() {
    var database = firebase.database().ref();
    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('content');
    var Today = new Date();
    var user = firebase.auth().currentUser;
    var $show = $('#show');
    $('#content').on('keydown', function (e) {
        if (e.keyCode == 13) {
            writeData();
        }
    });
    function writeData() {

        if (post_txt.value != "") {
            if (post_txt.value[0] == "@" || post_txt.value[0] == "+") {
                RoomCode = "";
                for (var i = 0; i < post_txt.value.length - 1; i++) {
                    RoomCode += post_txt.value[i + 1];
                }
                alert('Your are in room ' + RoomCode + ' now.');
                post_txt.value = "";
            }
            else {
                var Renew = new Date();
                var newpostref = firebase.database().ref(RoomCode).push();
                newpostref.set({
                    email: firebase.auth().currentUser.displayName,
                    data: post_txt.value,
                    photo: firebase.auth().currentUser.photoURL,
                    Year: Renew.getFullYear(),
                    Month: Renew.getMonth() + 1,
                    Day: Renew.getDate(),
                    Hours: Renew.getHours(),
                    Minutes: Renew.getMinutes(),
                    Seconds: Renew.getSeconds(),
                    UID: firebase.auth().currentUser.uid
                });
                post_txt.value = "";
            }

        }
        console.log('RoomCode is' + RoomCode);
    }
    post_btn.addEventListener('click', function () {

        if (post_txt.value != "") {
            if (post_txt.value[0] == "@" || post_txt.value[0] == "+") {
                RoomCode = "";
                for (var i = 0; i < post_txt.value.length - 1; i++) {
                    RoomCode += post_txt.value[i + 1];
                }
                alert('Your are in room ' + RoomCode + ' now.');
                post_txt.value = "";
            } else {
                var newpostref = firebase.database().ref(RoomCode).push();
                newpostref.set({
                    email: firebase.auth().currentUser.displayName,
                    data: post_txt.value,
                    photo: firebase.auth().currentUser.photoURL,
                    Year: Today.getFullYear(),
                    Month: Today.getMonth() + 1,
                    Day: Today.getDate(),
                    Hours: Today.getHours(),
                    Minutes: Today.getMinutes(),
                    Seconds: Today.getSeconds(),
                    UID: firebase.auth().currentUser.uid
                });
                post_txt.value = "";
            }

        }
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-white'><strong class='d-block text-white-dark'>";
    var str_after_content = "</p></div></div>\n<br>";
    console.log('ref之前的RoomCode是' + RoomCode);
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    firebase.database().ref(RoomCode).once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                console.log('載入歷史紀錄');
                var childData = childSnapshot.val();
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                if (childSnapshot.val().UID === firebase.auth().currentUser.uid) {
                    total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;float:right;'>" + "<a style='font-color:white;float:right;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</strong>" + "<a style='font-color:white;float:right;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + "</div>" + str_after_content;
                }
                else {
                    total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + "<a style='font-color:white;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</div>" + "</strong>" + "<a style='font-color:white;float:left;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + str_after_content;
                }
                first_count += 1;
            });
            document.getElementById('show').innerHTML = total_post.join('');
            $show.scrollTop($show[0].scrollHeight);

            //add listener
            firebase.database().ref(RoomCode).on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    if (childData.UID === firebase.auth().currentUser.uid) {
                        total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;float:right;'>" + "<a style='font-color:white;float:right;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</strong>" + "<a style='font-color:white;float:right;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + "</div>" + str_after_content;
                    }
                    else {
                        total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + "<a style='font-color:white;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;border-color:white;'>" + str_before_username + "</div>" + "</strong>" + "<a style='font-color:white;float:left;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + str_after_content;
                    }
                    document.getElementById('show').innerHTML = total_post.join('');
                    $show.scrollTop($show[0].scrollHeight);
                }
            });
        })
        .catch(e => console.log(e.message));
}
$(function () {
    var database = firebase.database().ref();
    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('content');
    var Today = new Date();
    var user = firebase.auth().currentUser;
    var $show = $('#show');
    $('#content').on('keydown', function (e) {
        if (e.keyCode == 13) {
            writeData();
        }
    });
    function writeData() {

        if (post_txt.value != "") {
            if (post_txt.value[0] == "@" || post_txt.value[0] == "+") {
                RoomCode = "";
                for (var i = 0; i < post_txt.value.length - 1; i++) {
                    RoomCode += post_txt.value[i + 1];
                }
                alert('Your are in room ' + RoomCode + ' now.');
                post_txt.value = "";
                ExchageRoomCode();
            }
            else {
                var Renew = new Date();
                var newpostref = firebase.database().ref(RoomCode).push();
                newpostref.set({
                    email: firebase.auth().currentUser.displayName,
                    data: post_txt.value,
                    photo: firebase.auth().currentUser.photoURL,
                    Year: Renew.getFullYear(),
                    Month: Renew.getMonth() + 1,
                    Day: Renew.getDate(),
                    Hours: Renew.getHours(),
                    Minutes: Renew.getMinutes(),
                    Seconds: Renew.getSeconds(),
                    UID: firebase.auth().currentUser.uid
                });
                post_txt.value = "";
            }

        }
        console.log('RoomCode is' + RoomCode);
    }
    post_btn.addEventListener('click', function () {

        if (post_txt.value != "") {
            if (post_txt.value[0] == "@" || post_txt.value[0] == "+") {
                RoomCode = "";
                for (var i = 0; i < post_txt.value.length - 1; i++) {
                    RoomCode += post_txt.value[i + 1];
                }
                alert('Your are in room ' + RoomCode + ' now.');
                post_txt.value = "";
                ExchageRoomCode();
            } else {
                var newpostref = firebase.database().ref(RoomCode).push();
                newpostref.set({
                    email: firebase.auth().currentUser.displayName,
                    data: post_txt.value,
                    photo: firebase.auth().currentUser.photoURL,
                    Year: Today.getFullYear(),
                    Month: Today.getMonth() + 1,
                    Day: Today.getDate(),
                    Hours: Today.getHours(),
                    Minutes: Today.getMinutes(),
                    Seconds: Today.getSeconds(),
                    UID: firebase.auth().currentUser.uid
                });
                post_txt.value = "";
            }

        }
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-white'><strong class='d-block text-white-dark'>";
    var str_after_content = "</p></div></div>\n<br>";
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    firebase.database().ref(RoomCode).once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                console.log('載入歷史紀錄');
                var childData = childSnapshot.val();
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                if (childSnapshot.val().UID === firebase.auth().currentUser.uid) {
                    total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;float:right;'>" + "<a style='font-color:white;float:right;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</strong>" + "<a style='font-color:white;float:right;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + "</div>" + str_after_content;
                }
                else {
                    total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + "<a style='font-color:white;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</div>" + "</strong>" + "<a style='font-color:white;float:left;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + str_after_content;
                }
                first_count += 1;
            });
            document.getElementById('show').innerHTML = total_post.join('');
            $show.scrollTop($show[0].scrollHeight);

            //add listener
            firebase.database().ref(RoomCode).on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    if (childData.UID === firebase.auth().currentUser.uid) {
                        total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;float:right;'>" + "<a style='font-color:white;float:right;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;'>" + str_before_username + "</strong>" + "<a style='font-color:white;float:right;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + "</div>" + str_after_content;
                    }
                    else {
                        total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + "<a style='font-color:white;'>" + childData.email + "</a>" + "<div style='text-align:left;font-size:16px;border-color:white;'>" + str_before_username + "</div>" + "</strong>" + "<a style='font-color:white;float:left;font-size:16px;border-color:white;border-style:solid;border-width:1px;border-radius: 5px;background-color:white;padding:10px 10px 10px 10px;cursor:pointer;' title='" + year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds + "'>" + childData.data + "</a>" + str_after_content;
                    }
                    document.getElementById('show').innerHTML = total_post.join('');
                    $show.scrollTop($show[0].scrollHeight);
                }
            });
        })
        .catch(e => console.log(e.message));
        var user_email = '';
        firebase.auth().onAuthStateChanged(function (user) {
            var menu = document.getElementById('dynamic-menu');
            if (user) {
                user_email = user.email;
                menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
                var logout_button = document.getElementById('logout-btn');
                logout_button.addEventListener('click', function () {
                    firebase.auth().signOut()
                        .then(function () {
                            alert('Sign Out!')
                            window.location.replace('chat.html')
                        })
                        .catch(function (error) {
                            alert('Sign Out Error!')
                        });
                });
            } else {
                menu.innerHTML = "<a class='dropdown-item' href='login.html'>Login</a>";
                document.getElementById('post_list').innerHTML = "";
            }
        });
});
var user_email = '';
firebase.auth().onAuthStateChanged(function (user) {
    var menu = document.getElementById('dynamic-menu');
    if (user) {
        user_email = user.email;
        menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        var logout_button = document.getElementById('logout-btn');
        logout_button.addEventListener('click', function () {
            firebase.auth().signOut()
                .then(function () {
                    alert('Sign Out!')
                    window.location.replace('chat.html')
                })
                .catch(function (error) {
                    alert('Sign Out Error!')
                });
        });
    } else {
        menu.innerHTML = "<a class='dropdown-item' href='login.html'>Login</a>";
        document.getElementById('post_list').innerHTML = "";
    }
});