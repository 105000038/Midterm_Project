﻿# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. chat room
    2. chat with new user
    3. load message history
    4. RWD
* Other functions (add/delete)
    1. 更改暱稱
    2. 忘記密碼可寄 reset password 的 email
    3. Sign up 建立帳號
    4. 第三方登入 (fb,google)
    5. google 通知
    6. css animation

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
1. 首頁(index.html)
<img src="index.jpg" width="500px" height="300px"></img><br>
</br>
(1)  背景為影片，這裡以截圖展示。
(2) 按 Start now 會到login的網頁。
(3) 點擊最底下的 “PaulShen”會跑出我的臉書。

2. 登入頁面
<img src="login.jpg" width="500px" height="300px"></img><br>
</br>
(1)  這裡可以登入帳號，點擊”New account”會跑到註冊的頁面。
(2) 可以第三方登入(Google,FB)
(3) 若忘記密碼，在”Emal address”打上你的Email，再點擊”Forgot Password”，就會寄重設密碼的信到你的信箱，點擊信件裡面的網址就可以重設密碼。
(4) Remember me 可以記住 Email 跟 Password。

3. 註冊頁面
<img src="signup.jpg" width="500px" height="300px"></img><br>
</br>
(1) 可以註冊帳號。
(2) 若有帳號了，點擊最後一行藍色字可以回登入頁面。

4. 聊天頁面(登入)
<img src="1.jpg" width="500px" height="300px"></img><br>
</br>
(1) 點擊Home 會回到首頁。
(2) 登入時會顯示聊天的歷史紀錄。
(3) Type message 然後 send 就可以即時聊天。
(4) 點Logout 可以登出。
(5) 會顯示名稱(若用google登入，會直接顯示你google帳號的名字)
(6) 會你顯示你送出訊息的時間。
(7) 會顯示你登入的email帳號。
(8) btn 會變色(css animation)。

5. Change Your ID
<img src="change.jpg" width="500px" height="300px"></img><br>
</br>
<img src="3.jpg" width="500px" height="300px"></img><br>
</br>
(1) 在change your id 那裡隨便打一個名字，按下button，就會跑出訊息”welcome+ 你更改的暱稱”，並且可以用暱稱聊天。

6. 登出
<img src="2.jpg" width="500px" height="300px"></img><br>
</br>
(1) 登出後看不到聊天的歷史紀錄。
Template
_____________________________________________________
1. w3school
2. 助教Lab06的example code(登入/登出)
3. coverr(背景影片)


使用到的API
____________________________________________________
基本跟Firebase有關的API都有使用到
另外還有Google & Facebook & "OneSignal"(google 通知)



## Security Report (Optional)
1. 當你沒登入時，你不能 read 和 write on firebase 
{
    "rules": {
        ".read": "auth != null",
        ".write": "auth != null"
    }
}
2. Look at the URL of the website. If it begins with “https” instead of “http” it means the site is secured using an SSL Certificate (the s stands for secure). SSL Certificates secure all of your data as it is passed from your browser to the website’s server. To get an SSL Certificate, the company must go through a validation process.

3. 當網頁以 http為開頭時，網站安全性是足夠的。

4. 左上角有顯示"安全"
